﻿using System;

namespace methods_parameters
{
    class Program
    {
        static void Main(string[] args)
        {
            int myNum = GetAnswer();
            Myloop(myNum);
        }
        public static int GetAnswer()
        {
            int num;
            Console.WriteLine("Skriv in ett nummer");
            num = int.Parse(Console.ReadLine());
            return num;
        }
        public static void Myloop(int num)
        {
            for (int i = 0; i < num; i++)
            {
                Console.WriteLine(i + 1);
            }
        }
    }
}
