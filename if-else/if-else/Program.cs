﻿using System;

namespace if_else
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;
            Console.WriteLine("Skriv in ett nummer");
            num = int.Parse(Console.ReadLine());
            if (num < 1)
            {
                Console.WriteLine("Skriv in ett större tal");
            }
            else
            {
                for (int i = 0; i < num; i++)
                {
                    Console.WriteLine(i + 1);
                }
            }
        }
    }
}
