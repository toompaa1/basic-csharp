﻿using System;
using System.Collections.Generic;

namespace arrays_loops
{
    class Program
    {
        static void Main(string[] args)
        {
            string taskOne = "Dricka";
            string taskTwo = "Äta";
            string taskTree = "Sova";
            List<string> taskList = new List<string>();
            string[] arrayList = new string[3];
            taskList.Add(taskOne);
            taskList.Add(taskTwo);
            taskList.Add(taskTree);
            arrayList[0] = taskOne;
            arrayList[1] = taskTwo;
            arrayList[2] = taskTree;
            foreach (string task in taskList)
            {
                Console.WriteLine(task);
            }
            foreach (string task in arrayList)
            {
                Console.WriteLine(task);
            }
        }
    }
}
