﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop_animal
{
    abstract class Animal
    {
        public string Name { get; set; }
        public int Weight { get; set; }
        public string Biome { get; set; }
        public abstract void MakeNoise();

        public void About()
        {
            Console.WriteLine($"Jag heter {Name}, jag väger {Weight}, jag lever i {Biome}");
        }

        public virtual void Sleep()
        {
            Console.WriteLine("This animal is sleeping");
        }

    }
}
