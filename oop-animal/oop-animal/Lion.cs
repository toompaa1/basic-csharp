﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop_animal
{
    class Lion : Animal
    {
        public override void Sleep()
        {
            Console.WriteLine("snarksnark");
        }

        public override void MakeNoise()
        {
            Console.WriteLine("RAWWWRR");
        }
    }
}
