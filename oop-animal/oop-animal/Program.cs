﻿using System;

namespace oop_animal
{
    class Program
    {
        static void Main(string[] args)
        {
            Shark babyShark = new Shark();
            Lion babyLion = new Lion();
            babyLion.Name = "Lejon";
            babyLion.Weight = 150;
            babyLion.Biome = "Desert";
            babyShark.Name = "Hajen";
            babyShark.Weight = 200;
            babyShark.Biome = "Ocean";
            babyShark.MakeNoise();
            babyLion.MakeNoise();
            babyLion.Sleep();
            babyShark.Sleep();
            babyShark.About();
            babyLion.About();

        }
    }
}
